#dotfiles
Just a random collection of config files for gnu/linux, Darwin, etc.

Also includes a python3 installer script. The script should operate
something like:
```
./install vim
```

Which would then create links in ${HOME} to correspond with files in the vim
directory, replacing the underscore with a dot.
